// Construimos servicios donde se recogeran los usuarios.
import { Injectable } from '@angular/core';
import { Usuarios } from '../models/usuario';

@Injectable({
  providedIn: 'root'
})
export class UsuariosService {

  constructor() { }

  // Esta función llamaría al POST, en este caso mockeamos los usuarios.
  getAllUsuarios(): any {
    return Usuarios;
  }

}