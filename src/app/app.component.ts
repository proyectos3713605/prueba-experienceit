import { Component } from '@angular/core';
import { Usuarios } from './models/usuario';
import { UsuariosService } from './services/getUsuarios';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  itemsPerPage = 5;
  currentPage = 1;
  searchText: any;
  test: any;
  data: any;
  
  constructor(private usuariosService: UsuariosService) {}

  //Llamada al servicio donde se recogen los usuarios

  get paginatedData() {
    const start = (this.currentPage -1) * (this.itemsPerPage);
    const end = start + this.itemsPerPage;
    return this.data.slice(start, end);
  }

  changePage(page: number) {
    this.currentPage = page;
  }

  ngOnInit() {
    this.data = this.usuariosService.getAllUsuarios();
  }

}
